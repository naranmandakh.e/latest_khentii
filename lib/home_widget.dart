import 'package:latest_khentii/screen/home_screen.dart';
import 'package:latest_khentii/screen/login_screen.dart';
import 'package:latest_khentii/screen/notif_list.dart';
import 'package:flutter/material.dart';
import 'package:latest_khentii/model/user.dart';
import 'package:latest_khentii/screen/profile_screen.dart';
import 'placeholder_widget.dart';
import 'package:latest_khentii/utils/util.dart';

class HomePage extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return _HomeState();
  }
}

class _HomeState extends State<HomePage> {

  int i = 0;
  var pages =
  [
    HomeScreen(),
    NotificationList(),
    LoginScreen()
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: pages[i],// new
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor : Color(0xff00389d),
        onTap: _onBottomNavBarTab, // new
        currentIndex: i, // new
        items: [
          new BottomNavigationBarItem(
            icon: Icon(Icons.home,color: Colors.white,),
            title: Text('Эхлэл',style: TextStyle(color: Colors.white),),

          ),
          new BottomNavigationBarItem(
            icon: Icon(Icons.notifications, color: Colors.white,),
            title: Text('Мэдэгдэл',style: TextStyle(color: Colors.white),),
          ),
          new BottomNavigationBarItem(
              icon: Icon(Icons.person, color: Colors.white,),
              title: Text('Миний цэс',style: TextStyle(color: Colors.white),),

          )
        ],

      ),
    );
  }

  void _onBottomNavBarTab(int index) async
  {
    if(index == 2){
      User user = await Util.getUserInfo();
      if(user != null && user.id.isNotEmpty){
        print(user.email);
        setState(() {
          pages[2] = ProfileScreen(switchToLogin: switchToScreen);
        });
      }else{
        pages[2] = LoginScreen(changeBottomBar: changeTabByIndex);
      }
    }

    setState(() {
      i = index;
    });
  }

  void switchToScreen(){
    setState(() {
      pages[2] = LoginScreen(changeBottomBar: changeTabByIndex,);
    });
  }

  void changeTabByIndex(int index){
    setState(() {
      i = index;
    });
  }
}

