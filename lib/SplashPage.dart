
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'home_widget.dart';
import 'placeholder_widget.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Ухаалаг хэнтий',
      home: HomePage(),
    );
  }
}

