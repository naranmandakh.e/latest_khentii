import 'package:json_annotation/json_annotation.dart';
part 'package:latest_khentii/model/user.g.dart';

@JsonSerializable(nullable : true)
class User{

  @JsonKey(name : 'id')
  String id;

  @JsonKey(name : 'lname')
  String lName;

  @JsonKey(name : 'fname')
  String fName;

  @JsonKey(name : 'image')
  String image;

  @JsonKey(name : 'email')
  String email;

  @JsonKey(name : 'phone')
  String phone;

  @JsonKey(name : 'created_date')
  String createdDate;

  @JsonKey(name : 'last_login')
  String lastLogin;

  @JsonKey(name : 'is_active')
  String isActive;

  User({
  this.id,
  this.fName,
  this.lName,
  this.image,
  this.email,
  this.phone,
  this.createdDate,
  this.lastLogin,
  this.isActive
});

factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
Map<String, dynamic> toJson() => _$UserToJson(this);
}
