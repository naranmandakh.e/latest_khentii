// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) {
  return User(
    id: json['id'] as String,
    fName: json['fname'] as String,
    lName: json['lname'] as String,
    image: json['image'] as String,
    email: json['email'] as String,
    phone: json['phone'] as String,
    createdDate: json['created_date'] as String,
    lastLogin: json['last_login'] as String,
    isActive: json['is_active'] as String,
  );
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'id': instance.id,
      'lname': instance.lName,
      'fname': instance.fName,
      'image': instance.image,
      'email': instance.email,
      'phone': instance.phone,
      'created_date': instance.createdDate,
      'last_login': instance.lastLogin,
      'is_active': instance.isActive,
    };
