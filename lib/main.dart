import 'package:flutter/material.dart';
import 'package:latest_khentii/screen/home_screen.dart';
import 'package:latest_khentii/screen/login_screen.dart';
import 'package:latest_khentii/screen/registration_screen.dart';
import 'package:latest_khentii/screen/splash_screen.dart';

import 'SplashPage.dart';

var routes = <String, WidgetBuilder>{
  "/RegistrationScreen": (BuildContext context) => RegistrationScreen(),
  "/LoginScreen": (BuildContext context) => LoginScreen(),
  "/HomeScreen": (BuildContext context) => HomeScreen(),
  "/HomePage": (BuildContext context) => Home(),


};

void main() => runApp(new MaterialApp(
    theme:
    ThemeData(primaryColor: Colors.blueAccent,
        primaryColorDark: Colors.blueAccent),
    debugShowCheckedModeBanner: false,
    home: SplashScreen(),
    routes: routes));