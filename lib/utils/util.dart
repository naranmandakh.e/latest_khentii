import 'package:latest_khentii/utils/list_item.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter/material.dart';
import 'package:latest_khentii/model/user.dart';
import 'dart:convert';

class Util {
  static const String name = "Цахим хэнтий";
  static const String store = "Online Store\n For Everyone";
  static const String skip = "SKIP";
  static const String next = "NEXT";
  static const String gotIt = "GOT IT";
  static String userName ="Хэнтий аймаг";
  static String emailId ="Засаг даргийн тамгийн газар";
  static String profilePic ="assets/splash.png";
  static String enumUserInfo = "userInfo";
  static List<String> descriptionList = new List<String>();
  static List<String> mediaList = new List<String>();
  static List<ListItem> listItems = new List<ListItem>();

  static Future<User> getUserInfo() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userStr = prefs.getString(enumUserInfo);
    if(userStr != null && userStr.isNotEmpty){
      return Future.value(User.fromJson(json.decode(userStr)));
    }
    return Future.value(null);
  }

  static Future<bool> logOut()async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove(enumUserInfo);
  }

  static showSuccessToast(String msg){

    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1,
        backgroundColor: Colors.green,
        textColor: Colors.white,
        fontSize: 16.0);
  }
}
