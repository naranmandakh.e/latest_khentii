import 'package:flutter/material.dart';
import 'package:latest_khentii/home_widget.dart';
class NavigationRouter {
  static void switchToLogin(BuildContext context) {
    Navigator.pushNamed(context, "/LoginScreen");
  }

  static void switchToRegistration(BuildContext context) {
    Navigator.pushNamed(context, "/RegistrationScreen");
  }

  static void switchToHome(BuildContext context) {
    Navigator.pushNamed(context, "/HomeScreen");
  }
  static void switchToProfile(BuildContext context) {
    Navigator.pushNamed(context, "/ProfileScreen");
  }
  static void switchToHomePage(BuildContext context) {
    Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => HomePage()), ModalRoute.withName("/HomePage"));
  }
  static void switchToUploadImage(BuildContext context) {
    Navigator.pushNamed(context, "/FilePickerDemo");
  }

}
