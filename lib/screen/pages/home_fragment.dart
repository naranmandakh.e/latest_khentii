import 'package:latest_khentii/home_widget.dart';
import 'package:latest_khentii/screen/home_screen.dart';
import 'package:latest_khentii/utils/navigation_router.dart';
import 'package:flutter/material.dart';
import 'package:latest_khentii/utils/util.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

import 'aylal_fragment.dart';
import 'contact_us_fragment.dart';
import 'khentii_fragment.dart';
import 'liceance_fragment.dart';
import 'news_fragment.dart';
import 'request_fragment.dart';

class HomeFragment extends StatefulWidget {
  List<String> list = Util.mediaList;
  List<String> listDe = Util.descriptionList;

  @override
  _HomeFragmentState createState() => new _HomeFragmentState();
}

class ListItems {
  String title;
  String mediaImage;

  ListItems(this.title, this.mediaImage);
}

class _HomeFragmentState extends State<HomeFragment> {
  final List<String> _listViewData = [
    "Үндсэн веб хуудас",
    "Аялал жуулчлалын веб",
    "Мэдээ мэдээлэл",
    "Тусгай зөвшөөрлийн хуудас",
    "Санал хүсэлт гомдол",
    "Холбоо барих",
  ];
  int _selectedIndex = 0;

  final String logo = 'assets/logo.png';

  @override
  Widget build(BuildContext context) {
    var drawerOptions = <Widget>[];
    for (var i = 0; i < widget.list.length; i++) {
      var d = widget.list[i];
      var l = "";
      if (widget.listDe[i] != null) {
        l = widget.listDe[i];
      } else {
        //l ="Test data";
      }
      print(l);
      drawerOptions.add(new Column(
        children: <Widget>[
          new Column(
            children: <Widget>[
              new Image.network(
                logo,
              ),
              new Text(
                l,
                style: new TextStyle(
                    color: Color(0xff00389d),
                    fontSize: 24.0,
                    fontWeight: FontWeight.bold),
              ),
              new Divider(
                color: Color(0xff00389d),
                height: 2.0,
              )
            ],
          )
          /* new ListTile(

                leading: new Image.network(
                    d,
                ),
                title: new Text(
                    "",
                    style: new TextStyle(
                        color: Colors.deepPurple,
                        fontWeight: FontWeight.bold
                    )),
                selected: i == _selectedIndex,
              //  onTap: () => _onSelectItem(i),
              ),*/
        ],
      ));
    }
    return new Scaffold(
      drawer: new Drawer(
        child: new ListView(
          children: <Widget>[
            new Column(
              children: drawerOptions,
            )
          ],
        ),
      ),
      backgroundColor: Color(0xff00389d),
      body: OrientationBuilder(
        builder: (context, orientation) {
          int count = 2;
          if (orientation == Orientation.landscape) {
            count = 3;
          }
          return GridView.count(
            crossAxisCount: count,
            crossAxisSpacing: 15.0,
            mainAxisSpacing: 15.0,
            childAspectRatio: 1.4,
            // padding: EdgeInsets.only(left: 28, right: 28, bottom: 38),
            padding: EdgeInsets.all(15.0),
            children: <Widget>[
//              RaisedButton.icon(
//                onPressed: () {},
//                elevation: 0.0,
//                label: Text(_listViewData.first, style: TextStyle(fontSize: 15.0),),textColor: Colors.white,
//                icon: new Icon(MdiIcons.globeModel),splashColor: Colors.white,
//                color: Colors.blue,
//                shape: RoundedRectangleBorder(borderRadius: new BorderRadius.circular(10.0))
//              ),
              GestureDetector(
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  color: Colors.blue,
                  child: Center(
                      child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 15.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Icon(MdiIcons.globeModel, color: Colors.white),
                        SizedBox(
                          width: 10.0,
                        ),
                        Expanded(
                          child: Text(_listViewData.first,
                              style: TextStyle(
                                  fontSize: 15.0,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                              textAlign: TextAlign.start),
                        )
                      ],
                    ),
                  )),
                ),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => KhentiiPage(),
                      ));
                },
              ),
              GestureDetector(
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  color: Colors.blue,
                  child: Center(
                      child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 15.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Icon(Icons.local_airport, color: Colors.white),
                        SizedBox(
                          width: 10.0,
                        ),
                        Expanded(
                          child: Text("Аялал жуулчлал",
                              style: TextStyle(
                                  fontSize: 15.0,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                              textAlign: TextAlign.start),
                        )
                      ],
                    ),
                  )),
                ),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => AylalPage(),
                      ));
                },
              ),
              GestureDetector(
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  color: Colors.blue,
                  child: Center(
                      child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 15.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Icon(MdiIcons.newspaper, color: Colors.white),
                        SizedBox(
                          width: 10.0,
                        ),
                        Expanded(
                          child: Text("Мэдээ мэдээлэл",
                              style: TextStyle(
                                  fontSize: 15.0,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                              textAlign: TextAlign.start),
                        )
                      ],
                    ),
                  )),
                ),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        //builder: (context) => NewsFragment(),
                        builder: (context) => LiceanceFragment(),

                      ));
                },
              ),
              GestureDetector(
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  color: Colors.blue,
                  child: Center(
                      child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 15.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Icon(MdiIcons.license, color: Colors.white),
                        SizedBox(
                          width: 10.0,
                        ),
                        Expanded(
                          child: Text("Тусгай зөвшөөрлийн хуудас",
                              style: TextStyle(
                                  fontSize: 15.0,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                              textAlign: TextAlign.start),
                        )
                      ],
                    ),
                  )),
                ),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => LiceanceFragment(),
                      ));
                },
              ),
              GestureDetector(
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  color: Colors.blue,
                  child: Center(
                      child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 15.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Icon(MdiIcons.comment, color: Colors.white),
                        SizedBox(
                          width: 10.0,
                        ),
                        Expanded(
                          child: Text("Санал хүсэлт гомдол",
                              style: TextStyle(
                                  fontSize: 15.0,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                              textAlign: TextAlign.start),
                        )
                      ],
                    ),
                  )),
                ),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => RequestFragment(),
                      ));
                },
              ),
              GestureDetector(
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  color: Colors.blue,
                  child: Center(
                      child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 15.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Icon(Icons.perm_contact_calendar, color: Colors.white),
                        SizedBox(
                          width: 10.0,
                        ),
                        Expanded(
                          child: Text("Холбоо барих",
                              style: TextStyle(
                                  fontSize: 15.0,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                              textAlign: TextAlign.start),
                        )
                      ],
                    ),
                  )),
                ),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        //builder: (context) => ContactUsFragment(),
                        builder: (context) => RequestFragment(),

                      ));
                },
              ),
//              RaisedButton.icon(
//                onPressed: () {},
//                label: Text("Аялал жуулчлалын веб", style: TextStyle(fontSize: 15.0), overflow: TextOverflow.clip,),textColor: Colors.white,
//                icon: Icon(Icons.local_airport),splashColor: Colors.white,
//                color: Colors.blueAccent,
//                  shape: RoundedRectangleBorder(borderRadius: new BorderRadius.circular(10.0))
//              ),
//              RaisedButton.icon(
//                onPressed: () {},
//                label: Text("Мэдээ мэдээлэл", style: TextStyle(fontSize: 15.0),),textColor: Colors.white,
//                icon: Icon(MdiIcons.newspaper),splashColor: Colors.white,
//                color: Colors.blueAccent,
//                  shape: RoundedRectangleBorder(borderRadius: new BorderRadius.circular(10.0))
//              ),
//              RaisedButton.icon(
//                onPressed: () {},
//                label: Text("Тусгай зөвшөөрлийн хуудас", style: TextStyle(fontSize: 15.0),),textColor: Colors.white,
//                icon: Icon(MdiIcons.license),splashColor: Colors.white,
//                color: Colors.blueAccent,
//                  shape: RoundedRectangleBorder(borderRadius: new BorderRadius.circular(10.0))
//              ),
//              RaisedButton.icon(
//                onPressed: () {},
//                label: Text("Санал хүсэлт гомдол", style: TextStyle(fontSize: 15.0),),textColor: Colors.white,
//                icon: Icon(MdiIcons.comment),splashColor: Colors.white,
//                color: Colors.blueAccent,
//                  shape: RoundedRectangleBorder(borderRadius: new BorderRadius.circular(10.0))
//              ),
//              RaisedButton.icon(
//                onPressed: () {},
//                label: Text("Холбоо барих", style: TextStyle(fontSize: 15.0)),textColor: Colors.white,
//                icon: Icon(Icons.perm_contact_calendar), splashColor: Colors.white,
//                color: Colors.blueAccent,
//                  shape: RoundedRectangleBorder(borderRadius: new BorderRadius.circular(10.0))
//              ),
            ],
          );
        },
      ),
    );
  }

  Future navigateToSubPage(context) async {
    // Navigator.push(context, MaterialPageRoute(builder: (context) => MyApp()));
  }
}
