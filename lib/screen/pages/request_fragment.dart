import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:fluttertoast/fluttertoast.dart';

class RequestFragment extends StatefulWidget {
  @override
  _RequestFragmentState createState() => new _RequestFragmentState();
}

class DrawerItem {
  String title;
  IconData icon;

  DrawerItem(this.title, this.icon);
}

class _RequestFragmentState extends State<RequestFragment> {
  String dropdownValue = 'Санал хүсэлт';
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController feedController = TextEditingController();
  bool isLoading = false;

  doSend() async {
    setState(() {
      isLoading = true;
    });
    Map data;
    if (nameController.text == "" ||
        phoneController.text == "" ||
        feedController.text == "") {
      Fluttertoast.showToast(
          msg: "Уучлаарай талбарын утгыг бөглөнө үү",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 1,
          backgroundColor: Colors.yellow,
          textColor: Colors.white,
          fontSize: 16.0);
    } else {
      String drop;
      if (dropdownValue == "Санал хүсэлт") {
        drop = "1";
      } else if (dropdownValue == "Гомдол") {
        drop = "2";
      } else {
        drop = "3";
      }
      data = {
        'type2': drop,
        'title': nameController.text,
        'phone': phoneController.text,
        'text': feedController.text,
      };
      await postRequest(data).then((String response) {
        if (response != null) {
          json.decode(response);
          print(response);
        }
      });

      setState(() {
        isLoading = false;
      });
    }
    ;
  }

  Future<String> postRequest(Map data) async {
    var url = 'http://www.khentii.itsolutions.mn/api/feedback';

    var response = await http
        .post(url,
            headers: {"Content-Type": "application/json"},
            body: json.encode(data))
        .catchError((err) {
      print(err);
    });

    print("${response.statusCode}");
    print("${response.body}");
    print(data);

    if (response.statusCode == 200) {
      Future.value(utf8.decode(response.bodyBytes));
      print(response.body);
      Fluttertoast.showToast(
          msg: "Таны сэтгэгдэл амжилттай илгээлээ",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 1,
          backgroundColor: Colors.green,
          textColor: Colors.white,
          fontSize: 16.0);
//      Timer(
//          Duration(seconds: 1),
//              () => Navigator.push(
//              context,
//              MaterialPageRoute(
//                builder: (context) => HomePage(),
//              )));
//      Navigator.push(
//          context,
//          MaterialPageRoute(
//            builder: (context) => HomePage(),
//          ));
    } else {
      Future.value(null);
    }
    return Future.value(null);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        backgroundColor: Color(0xff00389d),
        appBar: new AppBar(
          title: new Text("Санал хүсэлт"),
          backgroundColor: Color(0xff00389d),
          // elevation: defaultTargetPlatform== TargetPlatform.android?5.0:0.0,
        ),
        body: OrientationBuilder(builder: (context, orientation) {
          return ListView(padding: EdgeInsets.all(20.0), children: <Widget>[
            Container(
              padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 10.0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5.0),
                  border: Border.all(
                      color: Colors.black, style: BorderStyle.solid, width: 0.8),
                ),
                child: DropdownButton<String>(
                  value: dropdownValue,

//                  icon: Icon(Icons.arrow_drop_down_circle),
                  iconSize: 40,
                  underline: Container(
                    decoration: const BoxDecoration(
                        border: Border(bottom: BorderSide(color: Colors.transparent))),
                  ),
                  isDense: false,
                  elevation: 8,
                  style: (
                      TextStyle(
                    color: Colors.white,
                    backgroundColor: Color(0xff00389d),
                    decorationColor: Color(0xff00389d),
                  )),
                  onChanged: (String newValue) {
                    setState(() {
                      dropdownValue = newValue;
                    });
                  },
                  items: <String>['Санал хүсэлт', 'Гомдол', 'Санаачлага']
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value)

                    );
                  }).toList(),
                ),
              ),
            ),

            Container(
              padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
              child: TextField(
                controller: nameController,
                style: TextStyle(
                  color: Colors.white,
                ),
                decoration: InputDecoration(
                  border: OutlineInputBorder(borderSide: BorderSide(color: Colors.white)),
                  labelText: 'Гарчиг',
                  labelStyle: TextStyle(color: Colors.white),
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
              child: TextField(
                controller: phoneController,
                keyboardType: TextInputType.number,
                style: TextStyle(
                  color: Colors.white,
                ),
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Утас',
                  labelStyle: TextStyle(color: Colors.white),
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
              child: TextField(
                controller: feedController,
                maxLines: 5,
                style: TextStyle(
                  color: Colors.white,
                ),
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Санал хүсэлт, өргөдөл гомдол',
                  labelStyle: TextStyle(color: Colors.white),
                ),
                keyboardType: TextInputType.multiline,
              ),
            ),
            isLoading
                ? Center(
                    child: new Container(
                        margin: const EdgeInsets.only(top: 10.0),
                        child: new CircularProgressIndicator(
                          backgroundColor: Colors.white,
                        )))
                : Container(
                    height: 50.0,
                    width: 210.0,
                    margin: const EdgeInsets.symmetric(
                        horizontal: 20.0, vertical: 40.0),
                    child: new RaisedButton(
                      child: new Text(
                        'Илгээх',
                        style: new TextStyle(color: Colors.white),
                      ),
                      onPressed: () => doSend(),
                      color: Colors.blue,
                    ),
                  ),
          ]);
        }));
  }
}
