import 'package:latest_khentii/screen/License/License_viewer.dart';
import 'package:latest_khentii/screen/license/edu_license.dart';
import 'package:latest_khentii/screen/license/medic_license.dart';
import 'package:latest_khentii/screen/license/vodka_license.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class LiceanceFragment extends StatefulWidget {
  @override
  _LiceanceFragmentState createState() => new _LiceanceFragmentState();
}

class _LiceanceFragmentState extends State<LiceanceFragment> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        backgroundColor: Color(0xff00389d),
        appBar: new AppBar(
          title: new Text("Тусгай зөвшөөрөл"),
          backgroundColor: Color(0xff00389d),
         // elevation: defaultTargetPlatform== TargetPlatform.android?5.0:0.0,
        ),
        body: OrientationBuilder(builder: (context, orientation) {
          return ListView(
            padding: EdgeInsets.all(10.0),
            children: <Widget>[
              GestureDetector(
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  color: Colors.blue,
                  child: Center(
                      child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 15.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Icon(MdiIcons.chartHistogram, color: Colors.white),
                        SizedBox(
                          width: 10.0,
                          height: 100.0,
                        ),
                        Expanded(
                          child: Text(
                              "БОЛОВСРОЛЫН САЛБАРЫН ТУСГАЙ ЗӨВШӨӨРӨЛ АВАХ",
                              style: TextStyle(
                                  fontSize: 17.0,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                              textAlign: TextAlign.start),
                        )
                      ],
                    ),
                  )),
                ),
                onTap: () {

                  Navigator.push(
                  context,
                  MaterialPageRoute(
                  builder: (context) => EduLicensePage(),
                  ));

                },
              ),
              GestureDetector(
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  color: Colors.blue,
                  child: Center(
                      child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 15.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Icon(MdiIcons.chartHistogram, color: Colors.white),
                        SizedBox(
                          width: 10.0,
                          height: 100.0,
                        ),
                        Expanded(
                          child: Text(
                              "ЭРҮҮЛ МЭНДИЙН САЛБАРЫН ТУСГАЙ ЗӨВШӨӨРӨЛ АВАХ",
                              style: TextStyle(
                                  fontSize: 17.0,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                              textAlign: TextAlign.start),
                        )
                      ],
                    ),
                  )),
                ),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => MedicLicensePage(),
                      ));

                },
              ),
              GestureDetector(
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  color: Colors.blue,
                  child: Center(
                      child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 15.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Icon(MdiIcons.chartHistogram, color: Colors.white),
                        SizedBox(
                          width: 10.0,
                          height: 100.0,
                        ),
                        Expanded(
                          child: Text(
                              "СОГТУУРУУЛАХ УНДАА ХУДАЛДАХ, ТҮҮГЭЭР ҮЙЛЧЛЭХ ТУСГАЙ ЗӨВШӨӨРӨЛ АВАХ",
                              style: TextStyle(
                                  fontSize: 17.0,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                              textAlign: TextAlign.start),
                        )
                      ],
                    ),
                  )),
                ),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => VodkaLicensePage(),
                      ));

                },
              ),
              GestureDetector(
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  color: Colors.blue,
                  child: Center(
                      child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 15.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Icon(MdiIcons.chartHistogram, color: Colors.white),
                        SizedBox(
                          width: 10.0,
                          height: 100.0,
                        ),
                        Expanded(
                          child: Text("ТУСГАЙ ЗӨВШӨӨРЛИЙН ТУХАЙ ХАВСРАЛТУУД",
                              style: TextStyle(
                                  fontSize: 17.0,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                              textAlign: TextAlign.start),
                        )
                      ],
                    ),
                  )),
                ),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => LicenseViewer(),
                      ));

                },
              )
            ],
          );
        }));
  }
}
