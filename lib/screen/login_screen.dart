import 'dart:async';
import 'dart:convert';
import 'dart:io';
//import 'package:first_app/Db/User.dart';
import 'package:latest_khentii/screen/registration_screen.dart';
import 'package:flutter/material.dart';
import 'package:latest_khentii/utils/util.dart';
import 'package:latest_khentii/model/user.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
//import 'package:latest_khentii/screen/model/user.dart';

class LoginScreen extends StatefulWidget
{
  Function changeBottomBar;
  LoginScreen({Key key, @required this.changeBottomBar}) : super(key: key);

  @override
  _LoginScreenState createState() => new _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController nameController = TextEditingController();
  TextEditingController passController = TextEditingController();

  String _message = 'Log in/out by pressing the buttons below.';

  bool isLoading = false;

  doLogin() async
  {
    setState(() {
      isLoading = true;
    });

    Map data = {'email': nameController.text, 'password': passController.text};

    await checkLogin(data).then((String response) async
    {
      if (response != null)
      {
        User user = User.fromJson(json.decode(response));
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString(Util.enumUserInfo, response);

        if(user != null && user.id.isNotEmpty)
        {
          Util.showSuccessToast("Амжилттай нэвтэрлээ");
          widget.changeBottomBar(0);
        }
      }
    });

    setState(() {
      isLoading = false;
    });
  }

  Future<String> checkLogin(Map data) async
  {
    var url = 'https://travel.khentii.gov.mn/api/customerlogin';

    var response = await http
        .post(url,
            headers: {"Content-Type": "application/json"},
            body: json.encode(data))
        .catchError((err) {
          print(err);
        });

    print("${response.statusCode}");
    print("${response.body}");

    return (response.statusCode == 200) ? Future.value(utf8.decode(response.bodyBytes)) :  Future.value(null);
  }

  void _showMessage(String message) {
    setState(() {
      _message = message;
    });
  }

  String _validatePassword(String value) {
    if (value.length < 8) {
      return 'The Password must be at least 8 characters.';
    }

    return null;
  }

  String _validateEmail(String value) {
    if (!(value.length > 0 && value.contains("@") && value.contains("."))) {
      return 'The E-mail Address must be a valid email address.';
    }
    return null;
  }

  @override
  void initState() {
    // TODO: implement initState

//    nameController.text = 'migartr@gmail.com';
//    passController.text = 'qwerty';
  }

// void _submit() {
//   if (this._formKey.currentState.validate()) {
//     _formKey.currentState.save(); // Save our form now.
//
//     print('Printing the login data.');
//     print('Email: ${_data.email}');
//     print('Password: ${_data.password}');
//   }
// }
  @override
  Widget build(BuildContext context) {
    MediaQueryData media = MediaQuery.of(context);

    final Size screenSize = media.size;
    return new Scaffold(
      //key: this.scaffoldKey,
      appBar: new AppBar(
        title: new Text('Нэвтрэх'),
        backgroundColor: Color(0xff00389d),
      ),
      body: new Container(
          color: Color(0xff00389d),
          padding: new EdgeInsets.all(20.0),
          // color: Color(0xff00389d),
          child: new Form(
            //  key: this._formKey,
            child: new ListView(
              children: <Widget>[
                new Container(
                    padding: new EdgeInsets.all(20.0),
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        new Image.asset(
                          "assets/logo.png",
                          width: 150.0,
                          height: 150.0,
                        ),
                      ],
                    )),
                Container(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: TextFormField(
                      controller: nameController,
                      style: TextStyle(
                        color: Colors.white,
                      ),
                      keyboardType: TextInputType.emailAddress,
                      // Use email input type for emails.
                      decoration: InputDecoration(
                        hintText: 'you@example.com',
                        hintStyle: TextStyle(color: Colors.white),
                        labelText: 'Цахим шуудан',
                        labelStyle: TextStyle(color: Colors.white),
                        focusColor: Colors.white,
                        icon: Icon(Icons.email),
                      ),
                      validator: this._validateEmail,
                    )),
                new Container(
                  padding: const EdgeInsets.only(top: 10.0),
                  child: new TextFormField(
                    controller: passController,
                    style: TextStyle(
                      color: Colors.white,
                    ),
                    obscureText: true,
                    // Use secure text for passwords.
                    decoration: new InputDecoration(
                        hintText: 'Нууц үгээ оруулна уу',
                        hintStyle: TextStyle(color: Colors.white),
                        labelText: 'Нууц үг',
                        labelStyle: TextStyle(color: Colors.white),
                        icon: new Icon(Icons.lock)),
                    validator: this._validatePassword,
                  ),
                ),
                isLoading
                    ? Center(
                        child: new Container(
                            margin: const EdgeInsets.only(top: 20.0),
                            child: new CircularProgressIndicator(
                              backgroundColor: Colors.white,
                            )))
                    : Container(
                        width: screenSize.width,
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              height: 50.0,
                              margin:
                                  const EdgeInsets.only(left: 10.0, top: 30.0),
                              child: new RaisedButton(
                                child: new Text(
                                  'Нэвтрэх',
                                  style: new TextStyle(color: Colors.white),
                                ),
                                //onPressed: this._submit,
                                color: Colors.blue,
                                onPressed: doLogin,
                              ),
                            ),
                            new Container(
                              height: 50.0,
                              margin:
                                  const EdgeInsets.only(left: 20.0, top: 30.0),
                              child: new RaisedButton(
                                child: new Text(
                                  'Бүртгүүлэх',
                                  style: new TextStyle(color: Colors.white),
                                ),
                                onPressed: _navigateRegistration,
                                color: Colors.blueAccent,
                              ),
                            )
                          ],
                        ),
                      ),
              ],
            ),
          )),
    );
  }

  _navigateRegistration() {
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => RegistrationScreen(),
        ));
  }
}
