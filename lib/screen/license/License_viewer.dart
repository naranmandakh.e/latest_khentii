import 'package:flutter/material.dart';
import 'package:flutter_plugin_pdf_viewer/flutter_plugin_pdf_viewer.dart';
import 'package:latest_khentii/utils/util.dart';
import '../../home_widget.dart';
void main() => runApp(new LicenseViewer());

class LicenseViewer extends StatefulWidget {
  @override
  _LicenseViewerState createState() => _LicenseViewerState();

}

class _LicenseViewerState extends State<LicenseViewer> {

  bool _isLoading = false;
  final String logo = 'assets/logo.png';
  PDFDocument document;

  @override
  void initState() {
    super.initState();

    loadDocument();
  }

  loadDocument() async {

    document = await PDFDocument.fromURL('https://travel.khentii.gov.mn/uploads/files/2019/09/26/6dbfd7d850f3f5f9eef7243ae43d4661.pdf');

    setState(() => _isLoading = false);
  }

  changePDF(value) async {
    setState(() => _isLoading = false);
    if (value == 1) {
      document = await PDFDocument.fromURL('https://travel.khentii.gov.mn/uploads/files/2019/09/26/6dbfd7d850f3f5f9eef7243ae43d4661.pdf');
    } else if (value == 2) {
      document = await PDFDocument.fromURL(
          "https://travel.khentii.gov.mn/uploads/files/2019/09/26/926906fe60eb142005692d78450b4152.pdf");
    } else if (value == 3) {
      document = await PDFDocument.fromURL(
          "https://travel.khentii.gov.mn/uploads/files/2019/09/26/bb0bd81b71c67d90ae0147855d0d728b.pdf");
    } else {
      document = await PDFDocument.fromAsset('assets/sample.pdf');
    }
    setState(() => _isLoading = false);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        drawer: Drawer(
          child: Column(
            children: <Widget>[
              new UserAccountsDrawerHeader(
                decoration: BoxDecoration(
                  color:  Color(0xff00389d),
                ),
                accountName: new Text(Util.userName),
                accountEmail: new Text(Util.emailId),

                currentAccountPicture: new CircleAvatar(
                    maxRadius: 24.0,
                    backgroundColor: Color(0xff00389d),
                    child: new Center(
                      child: new Image.asset(
                        logo,
                        height: 150.0,
                        width: 150.0,
                      ),)
                  // backgroundImage: new Image.network(src),
                ),

              ),
              SizedBox(height: 36),
              ListTile(
                title: Text('БОЛОВСРОЛЫН ТУСГАЙ ЗӨВШӨӨРЛИЙН ГЭРЧИЛГЭЭ'),
                onTap: () {
                  changePDF(1);
                },
              ),
              ListTile(
                title: Text('ЭРҮҮЛ МЭНДИЙН ТУСГАЙ ЗӨВШӨӨРЛИЙН ГЭРЧИЛГЭЭ'),
                onTap: () {
                  changePDF(2);
                },
              ),
              ListTile(
                title: Text('СОГТУУРУУЛАХ УНДААНЫ ТУСГАЙ ЗӨВШӨӨРЛИЙН ГЭРЧИЛГЭЭ'),
                onTap: () {
                  changePDF(3);
                },
              ),
            ],
          ),
        ),
        appBar: AppBar(
          title: const Text('Хавсралт PDF файлууд'),
          backgroundColor: Color(0xff00389d),

        ),
        body: Center(

            child: _isLoading
                ? Center(child: CircularProgressIndicator())
                : PDFViewer(document: document)),
      ),
    );
  }
}