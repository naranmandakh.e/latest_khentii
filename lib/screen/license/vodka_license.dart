import 'dart:typed_data';

import 'package:latest_khentii/model/user.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:latest_khentii/utils/util.dart';

import '../home_screen.dart';
import '../login_screen.dart';
import '../notif_list.dart';

class VodkaLicensePage extends StatefulWidget {
  @override
  _VodkaLicensePage createState() => new _VodkaLicensePage();
}

class _VodkaLicensePage extends State<VodkaLicensePage> {
  List<Asset> images = List<Asset>();

  TextEditingController mainController = TextEditingController();
  TextEditingController reqtypeController = TextEditingController();
  TextEditingController nameController = TextEditingController();
//  String subController = 'Боловсролын байгууллага байгуулах';
  bool isLoading = false;
  String array;
  List imgs = [];
  @override
  void initState() {
    // TODO: implement initState
    mainController.text = 'Согтууруулах ундаа , түүгээр үйлчлэх';

//    passController.text = 'qwerty';
  }

  doSend() async {
    setState(() {
      isLoading = true;
    });
    Map data;
    if (mainController.text == "" ||
        reqtypeController.text == "" ||
        nameController.text == "" || imgs.length == 0) {
      Fluttertoast.showToast(
          msg: "Уучлаарай талбарын утгыг бөглөнө үү",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 1,
          backgroundColor: Colors.yellow,
          textColor: Colors.white,
          fontSize: 16.0);
    } else {

      data = {
        'main_type': "1",
        'sub_type': "",
        'name': nameController.text,
        'cus_id':"1",
        "req_type": reqtypeController.text,
        "ïmages": [imgs]
      };
      await postRequest(data).then((String response) {
        if (response != null) {
          json.decode(response);
          print(response);
        }
      });

      setState(() {
        isLoading = false;
      });
    }
    ;
  }

  Future<String> postRequest(Map data) async {
    var url = 'http://www.khentii.itsolutions.mn/api/license_req';

    var response = await http
        .post(url,
            headers: {"Content-Type": "application/json"},
            body: json.encode(data))
        .catchError((err) {
      print(err);
    });

    print("${response.statusCode}");
    print("${response.body}");
    print(data);

    if (response.statusCode == 200) {
      Future.value(utf8.decode(response.bodyBytes));
      print(response.body);
      Fluttertoast.showToast(
          msg: "Таны сэтгэгдэл амжилттай илгээлээ",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 1,
          backgroundColor: Colors.green,
          textColor: Colors.white,
          fontSize: 16.0);
//      Timer(
//          Duration(seconds: 1),
//              () => Navigator.push(
//              context,
//              MaterialPageRoute(
//                builder: (context) => HomePage(),
//              )));
//      Navigator.push(
//          context,
//          MaterialPageRoute(
//            builder: (context) => HomePage(),
//          ));
    } else {
      Future.value(null);
    }
    return Future.value(null);
  }

  @override
  Widget build(BuildContext context) {
    int i = 0;
    var pages =
    [
      HomeScreen(),
      NotificationList(),
      LoginScreen()
    ];

    MediaQueryData media = MediaQuery.of(context);

    final Size screenSize = media.size;
    return new Scaffold(
        appBar: new AppBar(
          title: new Text('Согтууруулах ундаа , түүгээр үйлчлэх'),
          backgroundColor: Color(0xff00389d),
        ),
        backgroundColor: Color(0xff00389d),
 //       body: pages[i],// new

        body: OrientationBuilder(builder: (context, orientation) {
          return ListView(padding: EdgeInsets.all(10.0), children: <Widget>[

          Container(
          padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
          child: TextField(
              controller: mainController,
              style: TextStyle(
                color: Colors.white,
              ),
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Салбар',
                labelStyle: TextStyle(color: Colors.white),
              ),
            ),),

          Container(
          padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
            child:TextField(
              controller: nameController,
              style: TextStyle(
                color: Colors.white,
              ),
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Хүсэлт гаргаж буй нэр',
                labelStyle: TextStyle(color: Colors.white),
              ),
            ),),

            RaisedButton(

              child: Text('Зурагнууд оруулах', style: TextStyle(color: Colors.white)),
              onPressed: loadAssets,
              color: Colors.blue,
            ),

//            TextField(
//              controller: emailController,
//              obscureText: true,
//              style: TextStyle(
//                color: Colors.white,
//              ),
//              decoration: InputDecoration(
//                border: OutlineInputBorder(),
//                labelText: 'Цахим шуудан',
//                labelStyle: TextStyle(color: Colors.white),
//              ),
//            ),

            isLoading
                ? Center(
                    child: new Container(
                        margin: const EdgeInsets.only(top: 20.0),
                        child: new CircularProgressIndicator(
                          backgroundColor: Colors.white,
                        )))
                : Container(
                    height: 50.0,
                    width: 210.0,
                    margin: const EdgeInsets.symmetric(
                        horizontal: 20.0, vertical: 40.0),
                    child: new RaisedButton(
                      child: new Text(
                        'Илгээх',
                        style: new TextStyle(color: Colors.white),
                      ),
                      onPressed: () => doSend(),
                      color: Colors.blue,
                    ),
                  ),

          ]);

        }));
//    bottomNavigationBar: BottomNavigationBar(
//      backgroundColor : Color(0xff00389d),
//      onTap: _onBottomNavBarTab, // new
//      currentIndex: i, // new
//      items: [
//        new BottomNavigationBarItem(
//          icon: Icon(Icons.home,color: Colors.white,),
//          title: Text('Эхлэл',style: TextStyle(color: Colors.white),),
//
//        ),
//        new BottomNavigationBarItem(
//          icon: Icon(Icons.notifications, color: Colors.white,),
//          title: Text('Мэдэгдэл',style: TextStyle(color: Colors.white),),
//        ),
//        new BottomNavigationBarItem(
//          icon: Icon(Icons.person, color: Colors.white,),
//          title: Text('Миний цэс',style: TextStyle(color: Colors.white),),
//
//        )
//      ],
//
//    );
//
  }


  Future<void> loadAssets() async {
    List<Asset> resultList = List<Asset>();

    String error = 'No Error Dectected';

    try {
      resultList = await MultiImagePicker.pickImages(maxImages: 300);

      for (var r in resultList) {
        print(r);
        print(r.name);
        ByteData aa = await r.requestOriginal();
        print(aa);
//        base64Encode(aa);
        Uint8List pngBytes = aa.buffer.asUint8List();
        String bs64 = base64Encode(pngBytes);
        String base64Str = base64.encode(pngBytes);

        print('base64: $base64Str');

        print('************************* ');
        print(pngBytes);
        print(bs64);
        print('base64: $base64Str');
        imgs.add(base64Str);
        print('=======================  array');
      }
    } on Exception catch (e) {
      error = e.toString();
      print(error);
    }

    if (!mounted) return;

    setState(() {
      images = resultList;
    });
  }
}
