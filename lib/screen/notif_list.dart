import 'package:flutter/material.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';

//void main() => runApp(new MyApp());
//
//class NotificationList extends StatelessWidget {
//  @override
//  Widget build(BuildContext context) {
//    return new MaterialApp(
//      title: 'Flutter Demo',
//      theme: new ThemeData(
//        primarySwatch: Colors.blue,
//      ),
//      home: new MyHomePage(title: 'Users'),
//    );
//  }
//}

class NotificationList extends StatefulWidget {
  NotificationList({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _NotificationListState createState() => new _NotificationListState();
}

class _NotificationListState extends State<NotificationList> {

  Future<List<Notif>> _getNotif() async {

    var data = await http.get("http://www.khentii.itsolutions.mn/api/notifnew/1");

    var jsonData = json.decode(data.body);

    List<Notif> users = [];

    for(var u in jsonData){

      Notif user = Notif(u["id"], u["text"], u["license_id"], u["cus_id"], u["is_new"], u["comment_id"]);

      users.add(user);

    }

    print(users.length);

    return users;

  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(widget.title),
      ),
      body: Container(
        child: FutureBuilder(
          future: _getNotif(),
          builder: (BuildContext context, AsyncSnapshot snapshot){
            print(snapshot.data);
            if(snapshot.data == null){
              return Container(
                  child: Center(
                      child: Text("Loading...")
                  )
              );
            } else {
              return ListView.builder(
                itemCount: snapshot.data.length,
                itemBuilder: (BuildContext context, int index) {
                  return ListTile(
                    leading: CircleAvatar(
                      backgroundImage: NetworkImage(
                        "assets/logo.png",
                      ),
                    ),
                    title: Text(snapshot.data[index].license_id),
                    subtitle: Text(snapshot.data[index].text),
                    onTap: (){

                      Navigator.push(context,
                          new MaterialPageRoute(builder: (context) => DetailPage(snapshot.data[index]))
                      );

                    },
                  );
                },
              );
            }
          },
        ),
      ),
    );
  }
}

class DetailPage extends StatelessWidget {

  final Notif notif;

  DetailPage(this.notif);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Мэдэгдэл"),
        )
    );
  }
}


class Notif {
  final int index;
  final String text;
  final String license_id;
  final String cus_id;
  final String is_new;
  final String comment_id;

  Notif(this.index, this.text, this.license_id, this.cus_id, this.is_new, this.comment_id);

}