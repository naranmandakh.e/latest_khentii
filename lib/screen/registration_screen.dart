import 'package:latest_khentii/home_widget.dart';
import 'package:flutter/material.dart';
import 'package:latest_khentii/utils/navigation_router.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:fluttertoast/fluttertoast.dart';

class RegistrationScreen extends StatefulWidget {
  @override
  _RegistrationScreenState createState() => new _RegistrationScreenState();
}

class _RegistrationScreenState extends State<RegistrationScreen> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passController = TextEditingController();
  TextEditingController repassController = TextEditingController();
  TextEditingController lnameController = TextEditingController();
  TextEditingController fnameController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  bool isLoading = false;

  doRegister() async {
    setState(() {
      isLoading = true;
    });
    Map data;
    if (repassController.text == passController.text) {
      data = {
        'email': emailController.text,
        'password': passController.text,
        'lname': lnameController.text,
        'fname': fnameController.text,
        'phone': phoneController.text
      };
    } else
      (Fluttertoast.showToast(
          msg: "Таны нууц үг таарахгүй байна",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 1,
          backgroundColor: Colors.yellow,
          textColor: Colors.white,
          fontSize: 16.0));

    await postRequest(data).then((String response) {
      if (response != null) {
        json.decode(response);
        print(response);
      }
    });

    setState(() {
      isLoading = false;
    });
  }

  Future<String> postRequest(Map data) async {
    var url = 'https://travel.khentii.gov.mn/api/registercustomer';

    var response = await http
        .post(url,
            headers: {"Content-Type": "application/json"},
            body: json.encode(data))
        .catchError((err) {
      print(err);
    });

    print("${response.statusCode}");
    print("${response.body}");

    if (response.statusCode == 200) {
      Future.value(utf8.decode(response.bodyBytes));
      Fluttertoast.showToast(
          msg: "Амжилттай бүртгэгдлээ",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 1,
          backgroundColor: Colors.green,
          textColor: Colors.white,
          fontSize: 16.0);
      Timer(
          Duration(seconds: 1),
          () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => HomePage(),
              )));
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => HomePage(),
          ));
    } else {
      Future.value(null);
    }
    return Future.value(null);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Бүртгүүлэх'),
        backgroundColor: Color(0xff00389d),
      ),
      body: new Container(
          padding: new EdgeInsets.all(15.0),
          color: Color(0xff00389d),
          child: new Form(
            child: new ListView(
              children: <Widget>[
//                new Container(
//                    padding: new EdgeInsets.all(15.0),
//                    child: new Row(
//                      mainAxisAlignment: MainAxisAlignment.center,
//                      children: <Widget>[
//                        new Image.asset(
//                          "assets/logo.png",
//                          width: 50.0,
//                          height: 50.0,
//                        ),
//                      ],
//                    )),
                new Container(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new TextFormField(
                        style: TextStyle(
                          color: Colors.white,
                        ),
                        controller: lnameController,
                        keyboardType: TextInputType.text,
                        // Use email input type for emails.
                        decoration: new InputDecoration(
                          hintText: 'Овог',
                          hintStyle: TextStyle(color: Colors.white),
                          labelText: 'Овог оруулна уу',
                          labelStyle: TextStyle(color: Colors.white),
                          focusColor: Colors.white,
                          icon: new Icon(Icons.person),
                        ))),
                new Container(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new TextFormField(
                        keyboardType: TextInputType.text,
                        controller: fnameController,
                        style: TextStyle(
                          color: Colors.white,
                        ),
                        // Use email input type for emails.
                        decoration: new InputDecoration(
                          hintText: 'Нэр',
                          hintStyle: TextStyle(color: Colors.white),
                          labelText: 'Нэр оруулна уу',
                          labelStyle: TextStyle(color: Colors.white),
                          focusColor: Colors.white,
                          icon: new Icon(Icons.person),
                        ))),
                new Container(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new TextFormField(
                      keyboardType: TextInputType.emailAddress,
                      controller: emailController,
                      style: TextStyle(
                        color: Colors.white,
                      ),
                      // Use email input type for emails.
                      decoration: new InputDecoration(
                          hintText: 'you@example.com',
                          hintStyle: TextStyle(color: Colors.white),
                          labelText: 'Цахим шуудан',
                          labelStyle: TextStyle(color: Colors.white),
                          focusColor: Colors.white,
                          icon: new Icon(Icons.email)),
                    )),
                new Container(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new TextFormField(
                      keyboardType: TextInputType.number,
                      controller: phoneController,
                      style: TextStyle(
                        color: Colors.white,
                      ),
                      // Use email input type for emails.
                      decoration: new InputDecoration(
                          hintText: 'Утасны дугаараа оруулна уу',
                          hintStyle: TextStyle(color: Colors.white),
                          labelText: 'Утасны дугаар',
                          labelStyle: TextStyle(color: Colors.white),
                          focusColor: Colors.white,
                          icon: new Icon(Icons.phone)),
                    )),
                new Container(
                  padding: const EdgeInsets.only(top: 10.0),
                  child: new TextFormField(
                      controller: passController,
                      style: TextStyle(
                        color: Colors.white,
                      ),
                      obscureText: true, // Use secure text for passwords.
                      decoration: new InputDecoration(
                          hintText: 'Нууц үгээ оруулна уу',
                          hintStyle: TextStyle(color: Colors.white),
                          labelText: 'Нууц үг',
                          labelStyle: TextStyle(color: Colors.white),
                          focusColor: Colors.white,
                          icon: new Icon(Icons.lock))),
                ),
                new Container(
                  padding: const EdgeInsets.only(top: 10.0),
                  child: new TextFormField(
                      controller: repassController,
                      style: TextStyle(
                        color: Colors.white,
                      ),
                      obscureText: true, // Use secure text for passwords.
                      decoration: new InputDecoration(
                          hintText: 'Нууц үг /давталт/',
                          hintStyle: TextStyle(color: Colors.white),
                          labelText: 'Нууц үг /давталт/ оруулна уу',
                          labelStyle: TextStyle(color: Colors.white),
                          focusColor: Colors.white,
                          icon: new Icon(Icons.lock))),
                ),
                new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    isLoading
                        ? Center(
                            child: new Container(
                                margin: const EdgeInsets.only(top: 20.0),
                                child: new CircularProgressIndicator(
                                  backgroundColor: Colors.white,
                                )))
                        : Container(
                            height: 50.0,
                            width: 210.0,
                            margin: const EdgeInsets.symmetric(
                                horizontal: 20.0, vertical: 40.0),
                            child: new RaisedButton(
                              child: new Text(
                                'Бүртгүүлэх',
                                style: new TextStyle(color: Colors.white),
                              ),
                              onPressed: () => doRegister(),
                              color: Colors.blue,
                            ),
                          ),
                  ],
                ),
              ],
            ),
          )),
    );
  }

  _performLogin() {}

  _navigateRegistration() {
    NavigationRouter.switchToRegistration(context);
  }
}
