import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';
import 'package:flutter/services.dart';
import 'dart:io';

//import 'package:first_app/Db/User.dart';
import 'package:flutter/material.dart';
import 'package:latest_khentii/utils/util.dart';
import 'package:multi_image_picker/multi_image_picker.dart';

class ProfileScreen extends StatefulWidget {
  Function switchToLogin;

  ProfileScreen({Key key, @required this.switchToLogin}) : super(key: key);

  @override
  _ProfileScreenState createState() => new _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  List<Asset> images = List<Asset>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    MediaQueryData media = MediaQuery.of(context);

    final Size screenSize = media.size;
    return new Scaffold(
      //key: this.scaffoldKey,
      appBar: new AppBar(
        title: new Text('Profile'),
        backgroundColor: Color(0xff00389d),
      ),
      body: new Container(
          color: Color(0xff00389d),
          padding: new EdgeInsets.all(20.0),
          // color: Color(0xff00389d),
          child: Center(
            child: Column(
              children: <Widget>[
                RaisedButton(
                  child: Text('logout', style: TextStyle(color: Colors.white)),
                  onPressed: () {
                    Util.logOut();
                    widget.switchToLogin();
                  },
                ),
                RaisedButton(
                  child:
                      Text('pickImages', style: TextStyle(color: Colors.white)),
                  onPressed: loadAssets,
                ),
              ],
            ),
          )),
    );
  }

  Future<void> loadAssets() async {
    List<Asset> resultList = List<Asset>();
    String error = 'No Error Dectected';

    try {
      resultList = await MultiImagePicker.pickImages(maxImages: 300);

      for (var r in resultList) {
        print(r);
        print(r.name);
        ByteData aa = await r.requestOriginal();
        print(aa);
//        base64Encode(aa);
        Uint8List pngBytes = aa.buffer.asUint8List();
        String bs64 = base64Encode(pngBytes);
        String base64Str = base64.encode(pngBytes);

        print('base64: $base64Str');

        print('************************* ');
        print(pngBytes);
        print(bs64);
        print('base64: $base64Str');
        print('=======================  ');
      }
    } on Exception catch (e) {
      error = e.toString();
      print(error);
    }

    if (!mounted) return;

    setState(() {
      images = resultList;
    });
  }
}
