
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:latest_khentii/screen/pages/aylal_fragment.dart';
import 'package:latest_khentii/screen/pages/contact_us_fragment.dart';
import 'package:latest_khentii/screen/pages/khentii_fragment.dart';
import 'package:latest_khentii/screen/pages/liceance_fragment.dart';
import 'package:latest_khentii/screen/pages/request_fragment.dart';
import 'package:latest_khentii/screen/pages/home_fragment.dart';

import 'package:latest_khentii/utils/util.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

import '../home_widget.dart';

class DrawerItem {
  String title;
  IconData icon;
  DrawerItem(this.title, this.icon);
}

class HomeScreen extends StatefulWidget {

  final drawerItems = [
    new DrawerItem("Эхлэл", Icons.home),
    new DrawerItem("Үндсэн веб", MdiIcons.globeModel),
    new DrawerItem("Аялалын жуулчлал", Icons.local_airport),
    new DrawerItem("Мэдээ мэдээлэл", MdiIcons.newspaper),
    new DrawerItem("Тусгай зөвшөөрөл", MdiIcons.license),
    new DrawerItem("Санал хүсэлт", Icons.info),
    new DrawerItem("Холбоо барих", Icons.contacts)
  ];
  @override
  _HomeScreenState createState() => new _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  int _selectedIndex = 0;
  final String logo = 'assets/logo.png';
  @override
  Widget build(BuildContext context) {

    var drawerOptions = <Widget>[];
    for (var i = 0; i < widget.drawerItems.length; i++) {
      var d = widget.drawerItems[i];
      drawerOptions.add(

          new Column(

            children: <Widget>[

              new ListTile(
                leading: new Icon(
                  d.icon,
                  color: Color(0xff00389d),
                ),
                title: new Text(
                    d.title,
                    style: new TextStyle(
                        color:  Color(0xff00389d),
                        fontWeight: FontWeight.bold
                    )),
                selected: i == _selectedIndex,
                onTap: () => _onSelectItem(i),
              ),
              new Divider(
                color: Colors.white,
                height: 1.0,
              )
            ],
          )


      );
    }
    return new Scaffold(
        appBar: new AppBar(
          title: new Text(widget.drawerItems[_selectedIndex].title),
          backgroundColor: Color(0xff00389d),
          elevation: defaultTargetPlatform== TargetPlatform.android?5.0:0.0,
        ),
        backgroundColor: Color(0xff00389d),
//        drawer: new Drawer(
//          child: new ListView(
//            children: <Widget>[
//
//              new UserAccountsDrawerHeader(
//                decoration: BoxDecoration(
//                  color:  Color(0xff00389d),
//                ),
//                accountName: new Text(Util.userName),
//                accountEmail: new Text(Util.emailId),
//
//                currentAccountPicture: new CircleAvatar(
//                    maxRadius: 24.0,
//                    backgroundColor: Color(0xff00389d),
//                    child: new Center(
//                      child: new Image.asset(
//                        logo,
//                        height: 150.0,
//                        width: 150.0,
//                      ),)
//                  // backgroundImage: new Image.network(src),
//                ),
//
//              ),
//              new Column(
//                  children:
//                  drawerOptions
//
//              )
//            ],
//          ),
//        ),
        body: _setDrawerItemWidget(_selectedIndex)

    );


  }

  _setDrawerItemWidget(int pos) {
    switch (pos) {
      case 0:
        return new HomeFragment();
      case 1:
        return new KhentiiPage();
      case 2:
        return new AylalPage();
      case 3:
      //  return new NewsFragment();
      case 4:
        return new LiceanceFragment();
      case 5:
        return new RequestFragment();
      case 6:
        return new ContactUsFragment();
      default:
        return new Text("Error");
    }
  }

  _onSelectItem(int index) {
    setState(() => _selectedIndex = index);
    Navigator.of(context).pop(); // close the drawer
  }


}
